var http = require("http"),
    url = require('url'),
    longPollingRequests = [],
    foreverIframeRequests = [],
    activexRequests = [],
    sseRequests = [],
    interactiveRequests = [],
    jsonpRequests = [],
    settings = {
        'hostname': '86.57.136.174',
        'scheme': 'http'
    };

var server = http.Server();
var io = require('socket.io')(server);
io.on('connection', function(socket){
    socket.on('event', function(data){});
    socket.on('disconnect', function(){});
});
server.listen(8006);

http.createServer(function(request, response) {
    longPollingRequests.push({
        response: response
    });
}).listen(8000);

http.createServer(function(request, response) {
    foreverIframeRequests.push({
        response: response
    });
}).listen(8001);

http.createServer(function(request, response) {
    response.writeHead(200, {
        "Content-Type": "text/event-stream",
        "Access-Control-Allow-Origin": "*"
    });
    sseRequests.push({
        response: response
    });
}).listen(8002);

http.createServer(function(request, response) {
    response.writeHead(200, {
        "Access-Control-Allow-Origin": "*"
    });
    interactiveRequests.push({
        response: response
    });
}).listen(8003);

http.createServer(function(request, response) {
    jsonpRequests.push({
        response: response,
        request: request
    });
}).listen(8004);

http.createServer(function(request, response) {
    var date = new Date();

    response.writeHead(200, {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        'Cache-Control': "no-cache"
    });

    response.end("message from server at " + date.toISOString())
}).listen(8005);

http.createServer(function(request, response) {
    activexRequests.push({
        response: response
    });
}).listen(8007);

var handleLongPollingRequests = function () {
    for (var i = longPollingRequests.length - 1; i >= 0; i--) {
        var response = longPollingRequests[i].response,
            date = new Date();

        response.writeHead(200, {
            "Content-Type": "text/plain",
            "Access-Control-Allow-Origin": "*",
            'Cache-Control': "no-cache"
        });

        response.end("message from server at " + date.toISOString());
    }
};

var handleActivexRequests = function () {
    for (var i = activexRequests.length - 1; i >= 0; i--) {
        var response = activexRequests[i].response,
            date = new Date();

        response.write("<script>parent.CallbackRegistry[window.name]('message from server at " + date.toISOString() + "')</script>");
    }
};

var handleForeverIframeRequests = function () {
    for (var i = foreverIframeRequests.length - 1; i >= 0; i--) {
        var response = foreverIframeRequests[i].response,
            date = new Date();

        response.write("<script>parent.postMessage('message from server at " + date.toISOString() + "', '" + settings.scheme + '://' + settings.hostname + "')</script>");
    }
};

var handleSseRequests = function () {
    for (var i = sseRequests.length - 1; i >= 0; i--) {
        var response = sseRequests[i].response,
            date = new Date(),
            message = 'event: ping\n' +
                      'retry: 10000\n' +
                      'data: message from server at ' + date.toISOString() + '\n' +
                      'id: ' + date.getTime() + '\n\n';

        response.write(message);
    }
};

var handleInteractiveRequests = function () {
    for (var i = interactiveRequests.length - 1; i >= 0; i--) {
        var response = interactiveRequests[i].response,
            date = new Date(),
            paddings = '';

        for(var j=0;j<2048;j++) paddings += ' ';

        response.write(paddings);
        response.write("message from server at " + date.toISOString());
    }
};

var handleJsonpRequests = function () {
    for (var i = jsonpRequests.length - 1; i >= 0; i--) {
        var response = jsonpRequests[i].response,
            request = jsonpRequests[i].request,
            url_parts = url.parse(request.url, true),
            callbackName = url_parts.query.callback,
            date = new Date();

        response.writeHead(200, {
            "Content-Type": "application/javascript"
        });
        response.end(callbackName + "('message from server at " + date.toISOString() + "');");
    }
};

var handleWebsocketsRequests = function () {
    var date = new Date();
    io.emit('ping', "message from server at " + date.toISOString())
};

setInterval(function() {
    handleLongPollingRequests();
    handleForeverIframeRequests();
    handleActivexRequests();
    handleSseRequests();
    handleInteractiveRequests();
    handleJsonpRequests();
    handleWebsocketsRequests();
}, 2000);
